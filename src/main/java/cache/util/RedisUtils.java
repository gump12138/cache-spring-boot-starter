package cache.util;


import static cache.domain.Constant.CHAR_SET;

import cache.helper.SpringContextHelper;
import java.time.Duration;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import lombok.extern.slf4j.Slf4j;
import org.redisson.Redisson;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.redisson.client.codec.StringCodec;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.CollectionUtils;

/**
 * @description: Redis Utils
 * @author: zhangzc
 * @modified By: zhangzc
 * @date: Created in 2022/7/8 11:50
 * @version:v1.0
 */
@Slf4j
public class RedisUtils {

    public RedisUtils() {
    }

    private static final StringRedisTemplate redisTemplate = SpringContextHelper
        .getBean("stringRedisTemplate", StringRedisTemplate.class);

    private static final RedissonClient redissonClient = SpringContextHelper
        .getBean("redisson", Redisson.class);

    /**
     * 设置有效时间
     *
     * @param key     Redis键
     * @param timeout 超时时间
     * @return true=设置成功；false=设置失败
     */
    public static boolean expire(final String key, final long timeout) {
        return expire(key, timeout, TimeUnit.SECONDS);
    }

    /**
     * 设置有效时间
     *
     * @param key     Redis键
     * @param timeout 超时时间
     * @param unit    时间单位
     * @return true=设置成功；false=设置失败
     */
    public static boolean expire(final String key, final long timeout, final TimeUnit unit) {
        Boolean ret = redisTemplate.expire(key, timeout, unit);
        return ret != null && ret;
    }

    /**
     * 删除单个key
     *
     * @param key 键
     * @return true=删除成功；false=删除失败
     */
    public static boolean del(final String key) {
        Boolean ret = redisTemplate.delete(key);
        return ret != null && ret;
    }

    /**
     * 删除多个key
     *
     * @param keys 键集合
     * @return 成功删除的个数
     */
    public static long del(final Collection<String> keys) {
        Long ret = redisTemplate.delete(keys);
        return ret == null ? 0 : ret;
    }

    /**
     * 存入普通对象
     *
     * @param key   Redis键
     * @param value 值
     */
    public static void set(final String key, final String value) {
        redisTemplate.opsForValue().set(key, value, 1, TimeUnit.MINUTES);
    }

    /**
     * 存入普通对象
     *
     * @param key     键
     * @param value   值
     * @param timeout 有效期，单位秒
     */
    public static void set(final String key, final String value, final long timeout) {
        redisTemplate.opsForValue().set(key, value, timeout, TimeUnit.SECONDS);
    }

    /**
     * 获取普通对象
     *
     * @param key 键
     * @return 对象
     */
    public static Object get(final String key) {
        return redisTemplate.opsForValue().get(key);
    }

    // -------------- hash操作start ------------

    /**
     * 往Hash中存入数据
     *
     * @param key   Redis键
     * @param hKey  Hash键
     * @param value 值
     */
    public static void hPut(final String key, final String hKey, final Object value) {
        redisTemplate.opsForHash().put(key, hKey, value);
    }

    /**
     * 往Hash中存入数据 并设置过期时间
     *
     * @param key      Redis键
     * @param hKey     Hash键
     * @param value    值
     * @param duration ttl
     */
    public static void hPut(final String key, final String hKey, final String value, Duration duration) {
        RMap<String, String> map = redissonClient.getMap(key, new StringCodec(CHAR_SET));
        map.put(hKey, value);
        map.expire(duration.getSeconds(), TimeUnit.SECONDS);
    }

    /**
     * 往hash存入整个hash
     *
     * @param key      RedisKey
     * @param values   valueMap
     * @param duration ttl
     */
    public static void hPutAll(final String key, final Map<String, String> values, Duration duration) {
        RMap<String, String> map = redissonClient.getMap(key, new StringCodec(CHAR_SET));
        map.putAll(values);
        map.expire(duration.getSeconds(), TimeUnit.SECONDS);
    }

    /**
     * 往Hash中存入多个数据
     *
     * @param key    Redis键
     * @param values Hash键值对
     */
    public static void hPutAll(final String key, final Map<String, Object> values) {
        redisTemplate.opsForHash().putAll(key, values);
    }

    /**
     * 获取Hash中的数据
     *
     * @param key  Redis键
     * @param hKey Hash键
     * @return Hash中的对象
     */
    public static Object hGet(final String key, final String hKey) {
        return redisTemplate.opsForHash().get(key, hKey);
    }

    /**
     * 获取多个Hash中的数据
     *
     * @param key   Redis键
     * @param hKeys Hash键集合
     * @return Hash对象集合
     */
    public static List<Object> hMultiGet(final String key, final Collection<Object> hKeys) {
        return redisTemplate.opsForHash().multiGet(key, hKeys);
    }

    /**
     * 获取hash中所有的数据
     *
     * @param key Redis键
     * @return HashMap
     */
    public static Map<String, String> hGetall(String key) {
        return redisTemplate.execute((RedisCallback<Map<String, String>>) con -> {
            Map<byte[], byte[]> result = con.hGetAll(key.getBytes());
            if (CollectionUtils.isEmpty(result)) {
                return new HashMap<>(0);
            }

            Map<String, String> ans = new HashMap<>(result.size());
            for (Map.Entry<byte[], byte[]> entry : result.entrySet()) {
                ans.put(new String(entry.getKey()), new String(entry.getValue()));
            }
            return ans;
        });
    }

    /**
     * 指定hash中hKey删除 可多key
     *
     * @param key  Redis键
     * @param hKey Hash键
     */
    public static void hDel(final String key, final String hKey) {
        if (hKey == null) {
            del(key);
            if (log.isDebugEnabled()) {
                log.info("删除整个hash,RKey={}", key);
            }
            return;
        }
        redisTemplate.opsForHash().delete(key, hKey);
    }

    /**
     * 指定RedisKey删除 整个hash
     *
     * @param key RedisKey
     */
    public static void hDelAll(final String key) {
        redisTemplate.opsForHash().delete(key);
    }
    // -------------- hash操作end ------------

//    public Map<String, String> hmget(String key, List<String> fields) {
//        List<String> result = redisTemplate.<String, String>opsForHash().multiGet(key, fields);
//        Map<String, String> ans = new HashMap<>(fields.size());
//        int index = -1;
//        for (String field : fields) {
//            ++index;
//            if (result.get(index) == null) {
//                continue;
//            }
//            ans.put(field, result.get(index));
//        }
//        return ans;
//    }

    /**
     * 往Set中存入数据
     *
     * @param key    Redis键
     * @param values 值
     * @return 存入的个数
     */
    public static long sSet(final String key, final String... values) {
        Long count = redisTemplate.opsForSet().add(key, values);
        return count == null ? 0 : count;
    }

    /**
     * 删除Set中的数据
     *
     * @param key    Redis键
     * @param values 值
     * @return 移除的个数
     */
    public static long sDel(final String key, final Object... values) {
        Long count = redisTemplate.opsForSet().remove(key, values);
        return count == null ? 0 : count;
    }

    /**
     * 往List中存入数据
     *
     * @param key   Redis键
     * @param value 数据
     * @return 存入的个数
     */
    public static long lPush(final String key, final String value) {
        Long count = redisTemplate.opsForList().rightPush(key, value);
        return count == null ? 0 : count;
    }

    /**
     * 往List中存入多个数据
     *
     * @param key    Redis键
     * @param values 多个数据
     * @return 存入的个数
     */
    public static long lPushAll(final String key, final Collection<String> values) {
        Long count = redisTemplate.opsForList().rightPushAll(key, values);
        return count == null ? 0 : count;
    }

    /**
     * 往List中存入多个数据
     *
     * @param key    Redis键
     * @param values 多个数据
     * @return 存入的个数
     */
    public static long lPushAll(final String key, final String... values) {
        Long count = redisTemplate.opsForList().rightPushAll(key, values);
        return count == null ? 0 : count;
    }

    /**
     * 从List中获取begin到end之间的元素
     *
     * @param key   Redis键
     * @param start 开始位置
     * @param end   结束位置（start=0，end=-1表示获取全部元素）
     * @return List对象
     */
    public static List<String> lGet(final String key, final int start, final int end) {
        return redisTemplate.opsForList().range(key, start, end);
    }

}
