package cache.rediscache;


import static cache.domain.Constant.CHAR_SET;

import cache.domain.Constant;
import cache.util.RedisUtils;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.concurrent.Callable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.SneakyThrows;
import org.springframework.cache.support.AbstractValueAdaptingCache;
import org.springframework.cache.support.NullValue;
import org.springframework.core.convert.ConversionFailedException;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.data.redis.cache.RedisCache;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.util.ByteUtils;
import org.springframework.lang.Nullable;
import org.springframework.util.Assert;
import org.springframework.util.ObjectUtils;
import org.springframework.util.ReflectionUtils;

/**
 * @description: 自定义cache 用于缓存Redis hash数据
 * @author: zhangzc
 * @modified By: zhangzc
 * @date: Created in 2022/7/13 11:07
 * @version:v1.0
 * @see RedisCache
 */
public class RedisHashCache extends AbstractValueAdaptingCache {

    private static final byte[] BINARY_NULL_VALUE = RedisSerializer.java().serialize(NullValue.INSTANCE);

    /**
     * 缓存name
     * <p>
     * 现在只有: constraints
     * </p>
     */
    private final String name;

    /**
     * redis config
     */
    private final RedisCacheConfiguration cacheConfig;
    private final ConversionService conversionService;


    public RedisHashCache(String name, RedisCacheConfiguration redisCacheConfiguration) {
        super(true);

        Assert.notNull(name, "Name must not be null");

        this.name = name;
        this.cacheConfig = redisCacheConfiguration;
        this.conversionService = cacheConfig.getConversionService();

        Assert.notNull(cacheConfig, "CacheConfig must not be null");
    }

    /**
     * @param key the key whose associated value is to be returned
     * @return Object
     * @see AbstractValueAdaptingCache#get(Object)
     */
    @SneakyThrows
    @Override
    protected Object lookup(Object key) {
        HashKey hashKey = convertHashKey(key);

        if (hashKey.getHKey() == null) {
            Map<String, String> valueMap = RedisUtils.hGetall(hashKey.getRKey());
            if (valueMap.isEmpty()) {
                return null;
            }
            Map<Object, Object> convertedCacheValueMap = new HashMap<>();
            for (Entry<String, String> entry : valueMap.entrySet()) {
                // todo key只限制为了Long 待修改
                convertedCacheValueMap.put(Long.valueOf(entry.getKey()), deserializeCacheValue(entry.getValue().getBytes()));
            }

            return convertedCacheValueMap;


        }
        Object value = RedisUtils.hGet(hashKey.getRKey(), hashKey.getHKey());
        if (value == null) {
            return null;
        }

        return deserializeCacheValue(((String) value).getBytes(CHAR_SET));
    }

    /**
     * Serialize the value to cache.
     *
     * @param value must not be {@literal null}.
     * @return never {@literal null}.
     */
    @SneakyThrows
    protected byte[] serializeCacheValue(Object value) {

        if (isAllowNullValues() && value instanceof NullValue) {
            return BINARY_NULL_VALUE;
        }

        return ByteUtils.getBytes(cacheConfig.getValueSerializationPair().write(value));
    }


    @Override
    public String getName() {
        return name;
    }

    @Override
    public Object getNativeCache() {
        throw new RuntimeException("暂时不支持");
    }

    @Override
    public synchronized <T> T get(Object key, Callable<T> valueLoader) {
        throw new RuntimeException("暂时不支持");
    }

    @Override
    public void put(Object key, Object value) {
        Object cacheValue = preProcessCacheValue(value);

        if (!isAllowNullValues() && cacheValue == null) {
            throw new IllegalArgumentException(String.format("Cache '%s' does not allow 'null' values. Avoid storing null via '@Cacheable(unless=\"#result == null\")' "
                + "or configure RedisHashCache to allow 'null' via RedisHashCacheConfiguration.", name));
        }

        if (cacheValue instanceof Map) {

            Map valueMap = (Map) value;

            Map<String, String> targetMap = new HashMap<>();
            valueMap.forEach((k, v) -> targetMap.put(covertKeyToString(k), covertAndSerializeValueToString(v)));

            RedisUtils.hPutAll(name + Constant.DOUBLE_COLON + convertKey(key), targetMap, cacheConfig.getTtl());

        } else {
            HashKey hashKey = convertHashKey(key);
            RedisUtils.hPut(hashKey.getRKey(), hashKey.getHKey(), covertAndSerializeValueToString(cacheValue), cacheConfig.getTtl());
        }


    }

    private String covertKeyToString(Object cacheRedisKey) {
        String hKey;
        try {
            hKey = String.valueOf(cacheRedisKey);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return hKey;
    }

    private String covertAndSerializeValueToString(Object cacheValue) {
        String hValue;
        try {
            hValue = new String(serializeCacheValue(cacheValue), CHAR_SET);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
        return hValue;
    }

    /**
     * Convert {@code key} to a {@link String} representation used for cache key creation.
     *
     * @param key will never be {@literal null}.
     * @return never {@literal null}.
     * @throws IllegalStateException if {@code key} cannot be converted to {@link String}.
     */
    protected String convertKey(Object key) {

        if (key instanceof String) {
            return (String) key;
        }

        TypeDescriptor source = TypeDescriptor.forObject(key);

        if (conversionService.canConvert(source, TypeDescriptor.valueOf(String.class))) {
            try {
                return conversionService.convert(key, String.class);
            } catch (ConversionFailedException e) {

                // may fail if the given key is a collection
                if (isCollectionLikeOrMap(source)) {
                    return convertCollectionLikeOrMapKey(key, source);
                }

                throw e;
            }
        }

        Method toString = ReflectionUtils.findMethod(key.getClass(), "toString");

        if (toString != null && !Object.class.equals(toString.getDeclaringClass())) {
            return key.toString();
        }

        throw new IllegalStateException(String.format(
            "Cannot convert cache key %s to String. Please register a suitable Converter via 'RedisCacheConfiguration.configureKeyConverters(...)' or override '%s.toString()'.",
            source, key.getClass().getSimpleName()));
    }

    private String convertCollectionLikeOrMapKey(Object key, TypeDescriptor source) {

        if (source.isMap()) {

            StringBuilder target = new StringBuilder("{");

            for (Entry<?, ?> entry : ((Map<?, ?>) key).entrySet()) {
                target.append(convertKey(entry.getKey())).append("=").append(convertKey(entry.getValue()));
            }
            target.append("}");

            return target.toString();
        } else if (source.isCollection() || source.isArray()) {

            StringJoiner sj = new StringJoiner(",");

            Collection<?> collection = source.isCollection() ? (Collection<?>) key : Arrays.asList(ObjectUtils.toObjectArray(key));

            for (Object val : collection) {
                sj.add(convertKey(val));
            }
            return "[" + sj.toString() + "]";
        }

        throw new IllegalArgumentException(String.format("Cannot convert cache key %s to String.", key));
    }

    private boolean isCollectionLikeOrMap(TypeDescriptor source) {
        return source.isArray() || source.isCollection() || source.isMap();
    }

    private String prefixCacheKey(String key) {

        // allow contextual cache names by computing the key prefix on every call.
        return cacheConfig.getKeyPrefixFor(name) + key;
    }

    /**
     * Customization hook called before passing object to
     * {@link org.springframework.data.redis.serializer.RedisSerializer}.
     *
     * @param value can be {@literal null}.
     * @return preprocessed value. Can be {@literal null}.
     */
    @Nullable
    protected Object preProcessCacheValue(@Nullable Object value) {

        if (value != null) {
            return value;
        }

        return isAllowNullValues() ? NullValue.INSTANCE : null;
    }

    @Override
    public ValueWrapper putIfAbsent(Object key, Object value) {
        throw new RuntimeException("暂时不实现");
    }

    @Override
    public void evict(Object key) {
        HashKey hashKey = convertHashKey(key);

        RedisUtils.hDel(hashKey.getRKey(), hashKey.getHKey());
    }

    /**
     * 暂定义 key= RedisKey:HashKey
     *
     * @param key key
     * @return HashKey
     */
    protected HashKey convertHashKey(Object key) {
        if (!(key instanceof String)) {
            throw new IllegalStateException(String.format("Cannot convert %s to String. Register a Converter or override toString().", key));
        }
        String[] keyArray = Optional.of((String) key).map(k -> k.split(Constant.COLON)).orElse(new String[0]);

        if (keyArray.length == 1) {
            return new HashKey(name + Constant.DOUBLE_COLON + keyArray[0]);
        }

        if (keyArray.length < 2) {
            throw new IllegalStateException(String.format("Cannot convert %s to String. Register a Converter or override toString().", key));
        }

        return new HashKey(name + Constant.DOUBLE_COLON + keyArray[0], keyArray[1]);
    }

    @Override
    public void clear() {

    }

    @Getter
    @AllArgsConstructor
    public static class HashKey {

        private String RKey;
        private String HKey;

        public HashKey(String RKey) {
            this.RKey = RKey;
        }
    }

    /**
     * Deserialize the given value to the actual cache value.
     *
     * @param value must not be {@literal null}.
     * @return can be {@literal null}.
     */
    @Nullable
    protected Object deserializeCacheValue(byte[] value) {

        if (isAllowNullValues() && ObjectUtils.nullSafeEquals(value, BINARY_NULL_VALUE)) {
            return NullValue.INSTANCE;
        }

        return cacheConfig.getValueSerializationPair().read(ByteBuffer.wrap(value));
    }

}
