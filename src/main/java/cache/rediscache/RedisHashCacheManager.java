package cache.rediscache;

import java.util.Collection;
import org.springframework.cache.Cache;
import org.springframework.cache.support.AbstractCacheManager;
import org.springframework.data.redis.cache.RedisCacheManager;

/**
 * @description: 用于Redis存取hash cache
 * @author: zhangzc
 * @modified By: zhangzc
 * @date: Created in 2022/7/13 11:52
 * @version:v1.0
 * @see RedisCacheManager
 */
public class RedisHashCacheManager extends AbstractCacheManager {

    public static final String BEAN_NAME = "redisHashCacheManager";

    private Collection<? extends RedisHashCache> caches;

    public void setCaches(Collection<? extends RedisHashCache> caches) {
        this.caches = caches;
    }

    public Collection<? extends RedisHashCache> getCaches() {
        return caches;
    }

    @Override
    protected Collection<? extends Cache> loadCaches() {
        return this.caches;
    }
}
